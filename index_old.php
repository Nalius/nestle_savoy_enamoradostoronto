<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Centinela</title>
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
    <script src="js/classie.js"></script>
    <script src="js/cbpAnimatedHeader.js"></script>
    <script src="js/jqBootstrapValidation.js"></script>
    <script src="js/contact_me.js"></script>



</head>
   <body id="page-top" class="index" onload="draw_canvas();">
    <div class="content_toronto">

	    	<div class="container">
	  			<div class="row">

					<!-- Modal -->
					<div id="myModal" class="modal fade" role="dialog">
					  <div class="modal-dialog">

					    <!-- Modal content-->
					    <div class="modal-content">
							<div class="modal-body">
					            <div class="col-md-10 col-md-offset-1">
					                <h2 class="text-center"><img src="img/hola.png"></h2>
					                <ul class="terminos col-md-12">    
					                <li>   Las personas deberán ser seguidoras del fan page de TORONTO® para poder  participar.</li> 

					                <li>   Los participantes deben ser mayores de 18 años.</li> 

					                <li>   Para participar, los usuarios tendrán que relatar “con detalles”, su historia romántica y enviárnoslo al correo:   ungrandetalle@gmail.com</li> 

					                <li>   TORONTO® tiene la potestad de descalificar imágenes/videos y comentarios indebidos que muestren cualquier actuación deshonesta (incluye contenido obsceno o agresivo); y actuaciones ofensivas hacia los demás participantes de TORONTO®.</li> 

					                <li>   Iremos recopilando las historias románticas, desde el 20 hasta el 03/02 de enero (15 días). Las historias que sean enviadas después del 04 de febrero, no serán admitidas.</li>  
					</li>
					                <li>   Una vez finalizado el periodo de recepción de historias (20 al 03/02) el equipo de Nestlé se encargará de elegir la historia ganadora.</li>  

					                <li>   Para la obtención de ciertos datos, el ganador tendrá que enviarnos sus datos para ser contactado y dirigirse a una entrevista en la agencia digital la WEB. Av. La Estancia, Centro Banaven (Cubo Negro) Torre D, Piso 3, Oficina D31-01, CHUAO, CARACAS – VENEZUELA.</li>

					                <li>   TORONTO®  se reserva el derecho a cancelar y/o modificar la actividad por causas mayores que afecten el buen desenvolvimiento de la actividad.</li>

					                <li>   TORONTO® se reserva el derecho de aclarar, complementar o modificar estos términos y condiciones, en cualquiera de sus partes y en cualquier momento.</li>

					                <li>   Las presentes bases y condiciones, como se describen en este instrumento, serán informadas al público a través de la cuenta oficial de la marca en Facebook.</li>

					                <li>   La participación y/o la aceptación de la premiación que se otorgará, implican para todos los efectos legales la total aceptación de estos términos y condiciones, por parte de las o los participantes.</li>

					                <li>   Esta actividad no aplica para empleados de la agencia digital la WEB y contratados de NESTLÉ® Venezuela, S.A.</li>

					                <li>  El ganador autoriza a TORONTO® a difundir y reproducir de forma gratuita en las Redes Sociales y demás medios de comunicación todo el material fijado voluntariamente en el sitio (fotografías y video), incluso para realizar actividades de material promocional o publicitario relacionadas con la marca TORONTO®.</li>

					                <li>   Facebook no patrocina, avala, ni administra de modo alguno esta actividad/evento, ni está asociado a él. Los participantes son conscientes que están proporcionando su información a TORONTO® y no a Facebook.</li> 

					                <li>   En caso de que los datos de contacto del ganador no sean correctos, TORONTO® no se hará responsable de la premiación. Y además de ello, si el ganador no responde por éste, elegiremos a otro inmediatamente.</li>

					                <li>   Ganar, está sujeto a todas las condiciones aquí especificadas. Todas las condiciones antes mencionadas deberán ser leídas y aceptadas por todos los participantes del concurso. Igualmente los términos y condiciones son públicos por lo que los usuarios podrán acceder y consultarlas cuando deseen.</li>
					                </ul>
					                <input type="checkbox" class="acepto"> He leído y estoy de acuerdo con los términos y condiciones.
					            </div>
					            <a class="boton_velo" id='descarga' href="" download='Carta_Enamorados'><img src="img/btn_enviar.png"></a>
					    </div>

					  </div>
					</div>
	  			</div>
	    		<div class="row">

	    			<div class="Logos_eslogan">
		                <img class="img-responsive center-block" src="img/logo_toronto.png">
		                <img class="img-responsive center-block slogan" src="img/eslogan.png">
		            </div>

		            <div class="text-center">
		            	<ul>
		            		<li class="letras">1. Escribe tu carta</li>
                			<li class="letras">2. Descargala</li> 
                			<li class="letras">3. Envíanosla a través de torontoungrandetalle@gmail.com</li>
		            	</ul>
		            </div>

		            <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1" style="padding:0;">
		            	<canvas id="carta" width="600px" height="222px" class="col-xs-12 col-md-10 col-md-offset-1"></canvas>
		
                		<textarea maxlength="1000" id="texto" onKeyup="func();" class="textarea_canvas"></textarea>
                		<a class="btn_terminos" data-toggle="modal" data-target="#myModal"><img src="img/btn_enviar.png"></a>

		            </div>

	    		</div>

	    	</div>

	    </div>
	    <script type="text/javascript">
	    	$(document).on("click",".boton_velo",function(){
            var a = $(".escribir").val();
            if($('.acepto').is(':checked')){
                window.location.href= "mailto:torontoungrandetalle@gmail.com?&amp;subject=Asunto%20email&amp;body="+a.replace(" ","%20");
                console.log("log");
            }else{
                return;               
            }
        });

        var img = new Image();
        var canvas_carta = document.getElementById('carta');
        ctx = canvas_carta.getContext("2d");
        
        function draw_canvas(){    
            img.src = "img/hojab.png";
            img.onload = function () {
                ctx.drawImage(img,0,0);
                func();
            }
        }

        function func(){
            var e = document.getElementById("texto").value,
            n = canvas_carta.getContext("2d");
            n.fillStyle="#990000";
            n.font="16px futura";
            n.textBaseline="top";
            n.fillText(e,150,0);
            var dato = canvas_carta.toDataURL("image/png");
            //dato = dato.replace("data:image/png;base64,","");
            $('#descarga').attr('href', dato);
        }

	    </script>
</body>
</html>
